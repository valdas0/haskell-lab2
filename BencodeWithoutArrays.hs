module BencodeWithoutArrays
where
import Data.List

type Move = (Int, Int, Char)
type Element = (Char, Move)
parseInt :: String -> (Int, String)
parseInt s =
  let
    number = takeWhile(/= 'e') s
    rest = drop (length number + 1) s
  in (read number, rest)

nextMove :: String -> String
nextMove s = "d" ++ nextMove' ((len, m) : parseList s) ++ "e"
  where (Just m) = move s
        len = head (show $ length $ parseList s)

nextMove' :: [Element] -> String
nextMove' [] = ""
nextMove' (e:es) =
  "1:"++[index]++"d1:v1:"++[m]++"1:xi"++show x++"e1:yi"++show y++"ee" ++ nextMove' es
  where (index, (x, y, m)) = e

move :: String -> Maybe Move
move s =
  let
    list = parseList s
    value = if null list then 'X'
      else let
        (_, (_, _, previousValue)) = last list
      in if isX previousValue then 'O' else 'X'
    answer = case findEmptySquare list of
        Nothing -> Nothing
        Just (x,y) -> Just (x, y, value)
  in answer

isX :: Char -> Bool
isX c = (c=='x') || (c=='X')

findEmptySquare :: [Element] -> Maybe (Int, Int)
findEmptySquare list
  | null emptySquares = Nothing
  | not $ null middle =  Just $ head middle
  | not $ null corners = Just $ head corners
  | otherwise = Just $ head emptySquares
  where squares = [(x,y)| x <- [0..2], y <- [0..2]]
        emptySquares = filter (\s -> all (not . isSameSquare s) list) squares
        middle = filter isMiddle emptySquares
        corners = filter isCorner emptySquares

isSameSquare :: (Int, Int) -> Element -> Bool
isSameSquare (x1, y1) (_, (x2, y2, _)) = (x1 == x2) && (y1 == y2)

parseList :: String -> [Element]
parseList s = sortBy elementCompare (parseElements $ drop 1 s)

parseElements :: String -> [Element]
parseElements "e" = []
parseElements s =
  let
    index = s !! 2
    value = s !! 9
    x = read [s !! 14]
    y = read [s !! 20]
    rest = drop 23 s
  in (index,(x, y, value)) : parseElements rest

elementCompare :: Element -> Element -> Ordering
elementCompare (value1, _) (value2, _)
  | value1 > value2 = GT
  | value1 < value2 = LT
  | otherwise = EQ

isMiddle :: (Int, Int) -> Bool
isMiddle (x, y) = (x==1) && (y==1)

isCorner :: (Int, Int) -> Bool
isCorner (x, y) = even x && even y
