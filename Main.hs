{-# LANGUAGE OverloadedStrings #-}
module Main
where
import BencodeWithArrays
import BencodeWithoutArrays
import Control.Lens.Operators
import Network.Wreq
import qualified Data.ByteString.Lazy.Char8 as BS
import Data.ByteString ()
import System.Environment

main :: IO ()
main = do
  args <- getArgs
  if length args == 2 then
    if head args == "post" then
      postTTT $ last args
    else if head args == "get" then
      getTTT $ last args
      else error "first parameter must be post or get"
  else error "pass two parameters"
  return ()


postTTT :: String ->  IO ()
postTTT gameId = do
  let url = getGameUrl gameId ++ "1"
  _ <- postMove optsPostP1 url (nextMove "de")
  r <- getMove optsGetP1 url
  _ <- postMove optsPostP1 url (nextMove r)
  r <- getMove optsGetP1 url
  _ <- postMove optsPostP1 url (nextMove r)
  r <- getMove optsGetP1 url
  _ <- postMove optsPostP1 url (nextMove r)
  r <- getMove optsGetP1 url
  _ <- postMove optsPostP1 url (nextMove r)
  return ()

getTTT :: String -> IO()
getTTT gameId = do
  let url = getGameUrl gameId ++ "2"
  r <- getMove optsGetP2 url
  _ <- postMove optsPostP2 url (nextMoveArr r)
  r <- getMove optsGetP2 url
  _ <- postMove optsPostP2 url (nextMoveArr r)
  r <- getMove optsGetP2 url
  _ <- postMove optsPostP2 url (nextMoveArr r)
  r <- getMove optsGetP2 url
  _ <- postMove optsPostP2 url (nextMoveArr r)
  return ()

optsPostP1 :: Options
optsPostP1 = defaults & header "Content-Type" .~ ["application/bencode+map"]
optsGetP1 :: Options
optsGetP1 = defaults & header "Accept" .~ ["application/bencode+map"]

optsPostP2 :: Options
optsPostP2 = defaults & header "Content-Type" .~ ["application/bencode+list"]
optsGetP2 :: Options
optsGetP2 = defaults & header "Accept" .~ ["application/bencode+list"]

postMove :: Options -> String -> String -> IO()
postMove opts url m = do
  _ <- postWith opts url (BS.pack m)
  return ()

getMove :: Options -> String -> IO String
getMove opts url = do
  r <- getWith opts url
  return $ BS.unpack $ r ^. responseBody

getGameUrl :: String -> String
getGameUrl gameId = "http://tictactoe.homedir.eu/game/"++gameId++"/player/"
