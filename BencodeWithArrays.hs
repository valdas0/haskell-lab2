module BencodeWithArrays
where

type MoveArr = (Int, Int, Char)
parseIntArr :: String -> (Int, String)
parseIntArr s =
  let
    number = takeWhile(/= 'e') s
    rest = drop (length number + 1) s
  in (read number, rest)

nextMoveArr :: String -> String
nextMoveArr s = "l" ++ nextMoveArr' (parseListArr s ++ [m]) ++ "e"
  where (Just m) = moveArr s

nextMoveArr' :: [MoveArr] -> String
nextMoveArr' [] = ""
nextMoveArr' (e:es) =
  "d1:v1:"++[m]++"1:xi"++show x++"e1:yi"++show y++"ee" ++ nextMoveArr' es
  where (x, y, m) = e

moveArr :: String -> Maybe MoveArr
moveArr s =
  let
    list = parseListArr s
    value = if null list then 'X'
      else let
        (_, _, previousValue) = last list
      in if isXArr previousValue then 'O' else 'X'
    answer = case findEmptySquareArr list of
        Nothing -> Nothing
        Just (x,y) -> Just (x, y, value)
  in answer

isXArr :: Char -> Bool
isXArr c = (c=='x') || (c=='X')

findEmptySquareArr :: [MoveArr] -> Maybe (Int, Int)
findEmptySquareArr list
  | null emptySquares = Nothing
  | not $ null middle =  Just $ head middle
  | not $ null corners = Just $ head corners
  | otherwise = Just $ last emptySquares
  where squares = [(x,y)| x <- [0..2], y <- [0..2]]
        emptySquares = filter (\s -> all (not . isSameSquareArr s) list) squares
        middle = filter isMiddle emptySquares
        corners = filter isCorner emptySquares

isSameSquareArr :: (Int, Int) -> MoveArr -> Bool
isSameSquareArr (x1, y1) (x2, y2, _) = (x1 == x2) && (y1 == y2)

parseListArr :: String -> [MoveArr]
parseListArr s = parseElementsArr $ drop 1 s

parseElementsArr :: String -> [MoveArr]
parseElementsArr "e" = []
parseElementsArr s =
  let
    value = s !! 6
    x = read [s !! 11]
    y = read [s !! 17]
    rest = drop 20 s
  in (x, y, value) : parseElementsArr rest


isMiddle :: (Int, Int) -> Bool
isMiddle (x, y) = (x==1) && (y==1)

isCorner :: (Int, Int) -> Bool
isCorner (x, y) = even x && even y
